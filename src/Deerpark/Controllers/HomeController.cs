﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Deerpark.Models;

namespace Deerpark.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [Route("get-in-touch")]
        public ActionResult GetInTouch()
        {
            return View();
        }

        [Route("get-in-touch"), HttpPost, ValidateAntiForgeryToken]
        public ActionResult GetInTouch(GetInTouch getInTouch)
        {
            if (ModelState.IsValid)
            {
                var toAddress = ConfigurationManager.AppSettings["ContactFormToAddress"];

                using (var emailClient = new SmtpClient())
                {
                    var message = new MailMessage
                    {
                        To = { new MailAddress(toAddress)},
                        From = new MailAddress("website@deerparkengineering.com"),
                        Subject = getInTouch.Subject,
                        Body = BuildEmailBody(getInTouch)
                    };

                    emailClient.Send(message);
                }

                ModelState.Clear();
                ViewBag.Success = true;
                ViewBag.Message = "Thank you, your message has been sent";
                return View(new GetInTouch());
            }

            ViewBag.Success = false;
            ViewBag.Message = "Sorry, your message could not be sent.  Please try again or phone us on XXXXXXXXXXX";
            return View(getInTouch);
        }

        private string BuildEmailBody(GetInTouch getInTouch)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("You have received a message from:");
            stringBuilder.AppendLine();
            stringBuilder.AppendLine(string.Format("Name: {0}", getInTouch.SenderName));
            stringBuilder.AppendLine(string.Format("Email Address: {0}", getInTouch.SenderEmail));
            stringBuilder.AppendLine();
            stringBuilder.AppendLine("Message:");
            stringBuilder.AppendLine(getInTouch.Content);

            return stringBuilder.ToString();
        }

        [Route("about-us")]
        public ActionResult About()
        {
            return View();
        }

        [Route("sustainable-corporate-and-social-responsibility")]
        public ActionResult SocialAndCorporate()
        {
            return View();
        }
    }
}