﻿using System.Web.Mvc;
using Deerpark.Models;

namespace Deerpark.Controllers
{
    [RoutePrefix("services")]
    public class ServicesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Route("control-panels")]
        public ActionResult ControlPanels()
        {
            var model = new Service
            {
                Name = "Control Panels"
            };

            return View("ControlPanels", model);
        }

        [Route("electrical-installation")]
        public ActionResult ElectricalInstallation()
        {
            var model = new Service
            {
                Name = "Electrical Installation & Testing"
            };

            return View("ElectricalInstallation", model);
        }

        [Route("wind")]
        public ActionResult Wind()
        {
            var model = new Service
            {
                Name = "Wind"            
            };

            return View("Wind", model);
        }

        [Route("mechanical-services")]
        public ActionResult MechanicalServices()
        {
            var model = new Service
            {
                Name = "Mechanical Services"
            };

            return View("MechanicalServices", model);
        }

        [Route("pumping-and-screening-equipment")]
        public ActionResult PumpingAndScreening()
        {
            var model = new Service
            {
                Name = "Pumping & Screening Equipment"
            };

            return View("PumpingAndScreening", model);
        }
    }
}