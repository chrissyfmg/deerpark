﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Deerpark.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static IHtmlString CssClass(this HtmlHelper helper, string value, bool condition)
        {
            if (condition)
            {
                return new HtmlString(string.Format("class=\"{0}\"", value));
            }

            return null;
        }
    }
}