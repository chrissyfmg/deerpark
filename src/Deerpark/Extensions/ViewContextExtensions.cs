﻿using System;
using System.Web.Mvc;

namespace Deerpark.Extensions
{
    public static class ViewContextExtensions
    {
        public static bool IsSpecificAction(this ViewContext viewContext, string actionName, string controllerName)
        {
            return viewContext.RouteData.GetRequiredString("action").Equals(actionName, StringComparison.InvariantCultureIgnoreCase)
                   && IsSpecificController(viewContext, controllerName);
        }

        public static bool IsSpecificController(this ViewContext viewContext, string controllerName)
        {
            return viewContext.RouteData.GetRequiredString("controller").Equals(controllerName, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}