﻿using Deerpark.Models.Validators;
using FluentValidation.Attributes;

namespace Deerpark.Models
{
    [Validator(typeof(GetInTouchValidator))]
    public class GetInTouch
    {
        public string Subject { get; set; }
        public string From { get; set; }
        public string Content { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
    }
}