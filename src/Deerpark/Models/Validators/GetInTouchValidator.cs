﻿using FluentValidation;

namespace Deerpark.Models.Validators
{
    public class GetInTouchValidator : AbstractValidator<GetInTouch>
    {
        public GetInTouchValidator()
        {
            RuleFor(x => x.Content).NotEmpty().WithMessage("Please enter a message")
                .Length(1, 2000).WithMessage("Please enter a shorter message (2000 characters)");

            RuleFor(x => x.SenderEmail).NotEmpty().WithMessage("Please enter your email address")
                .EmailAddress().WithMessage("Please enter a valid email address");

            RuleFor(x => x.Subject).NotEmpty().WithMessage("Please enter a subject")
                .Length(1, 200).WithMessage("Please enter a shorter subject (200 characters)");

            RuleFor(x => x.SenderName).NotEmpty().WithMessage("Please enter your name")
                .Length(0, 200).WithMessage("Please enter a shorter name (200 characters");
        }
    }
}