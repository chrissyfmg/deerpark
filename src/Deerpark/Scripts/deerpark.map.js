﻿var deerpark = deerpark || {}

deerpark.map = (function () {
    var position = { lat: 54.903530, lng: -5.990516 };

	return {
		initialize: function() {
		    var mapOptions = {
		        center: position,
		        zoom: 13,
		        disableDefaultUI: true,
		        styles: [
		            {
		                stylers: [
		                    { hue: "#003d77" },
		                    { saturation: -10 }
		                ]
		            }
		        ]
		    };

		    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

			var marker = new google.maps.Marker({
				map: map,
				draggable: true,
				animation: google.maps.Animation.DROP,
				position: position,
				title: 'Deerpark Engineering'
			});
		}
	};

}());

$(document).ready(function () {
	deerpark.map.initialize();
});